﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    // Enmigo Base Al cual se lo va a someter a decoraciones 
    class EnemigoBase : IEnemigo
    {
        // Establecemos el mensaje para el emigo base, es decir sin aramdura
        public override string Mensaje => "Enemigo sin armadura";
        //Asi mismo establecemos los puntos de daño recibido si este enemigo no tiene armadura
        public override int Dañorecibido => 10;
    }
}
