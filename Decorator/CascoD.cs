﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    //Componente casco de Enmigo Base ya que esto se lo puede o no añadir 
    class CascoD : DecoradorEnemigo
    {
        //primero hacemos el constructuor del componende del enemigo base para asi poder sobreescribir los datos 
        public CascoD(IEnemigo enemigo) : base(enemigo) { }
        //es decir estamos restandole los puntos de daño recibido por golpe ya que ya posee el componente casco lo que le otroga proteccion
        public override int Dañorecibido => enemigod.Dañorecibido-2;
        // en este caso estamos añadiendo una cadena de texto al mensaje base 
        public override string Mensaje => enemigod.Mensaje+", Con casco";
    }
}
