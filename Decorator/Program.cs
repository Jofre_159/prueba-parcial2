﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            int Daño;
            //creamos el enemigo como enemigobase es decir sin armadura para asi someterlo a un añadimiento de componentes mediante el decorador
            IEnemigo Enemigo = new EnemigoBase();
            //asignamos el valor del daño en una variable para comprobar cuanto daño es causado sobre el enmigo sin armadura
            Daño = Enemigo.Dañorecibido;
            //mostramos por pantalla los respectivos mensajes
            Console.WriteLine(Enemigo.Mensaje);
            Console.WriteLine(Daño);
            //aqui le añadimos el componente casco lo cual cambiara o sobreescribira los valores internos 
            Enemigo=new CascoD(Enemigo);
            //asignamos el valor del daño en una variable para comprobar cuanto daño es causado sobre el enmigo con casco
            Daño = Enemigo.Dañorecibido;
            //mostramos por pantalla los respectivos mensajes
            Console.WriteLine(Enemigo.Mensaje);
            Console.WriteLine(Daño);
            //aqui le añadimos el componente pechera luego del casco lo cual cambiara o sobreescribira los valores internos 
            Enemigo = new PecheraD(Enemigo);
            //asignamos el valor del daño en una variable para comprobar cuanto daño es causado sobre el enmigo con casco
            Daño = Enemigo.Dañorecibido;
            //mostramos por pantalla los respectivos mensajes
            Console.WriteLine(Enemigo.Mensaje);
            Console.WriteLine(Daño);
            //cabe recalcar que se le puede añadir solo uno, ninguno o varios ya que son independientes.
        }
    }
}
