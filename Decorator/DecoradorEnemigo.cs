﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    //El constructor de decorador Enemigo para asi implementar los componentes al enemigo base 
    public abstract class DecoradorEnemigo : IEnemigo
    {
        //se crea el constructor para asi que vaya pasando capas para añadir los componente
        protected IEnemigo enemigod;
        protected DecoradorEnemigo(IEnemigo enemigod)
        {
            this.enemigod = enemigod;
        }
    }
}
