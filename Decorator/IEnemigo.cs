﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    //Este es el enemigo abstrato ya que aqui vamos a declarar las propiedades y metodos 
    public abstract class IEnemigo
    {
        //propiedad MENSAJE lo que se va a cambiar dependiendo de los componentes añadidos 
        public abstract String Mensaje { get; }
        //Propiedad Dañorecibido lo que va a cambiar dependiendo de la decoracion a la cual sea sometida
        public abstract int Dañorecibido { get; }
    }
}
